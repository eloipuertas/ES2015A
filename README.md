# ES2015A
VideoJoc Curs Enginyeria Software 2015

[![Travis](https://secure.travis-ci.org/eloipuertas/ES2015A.png)](http://travis-ci.org/eloipuertas/ES2015A)

[Link a la web del joc](http://eloipuertas.github.io/ES2015A)

Configuració recomanada per desenvolupar i modificar aquest joc:
* Unity 5.2
* Windows:
  * Microsoft Visual C++ 2013 Redistributable (x64) - 12.0.30501

