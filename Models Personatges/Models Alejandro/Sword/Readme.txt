Thanks for downloading this sword model! You can get other free 3D models from my website, https://sites.google.com/site/westernroadmovies/
I sometimes even make models by request, so head over there if you want a custom model.

If you use this model, please credit me, and I'd also love to see whatever project you use it in.
Thanks,
John Ligtenberg