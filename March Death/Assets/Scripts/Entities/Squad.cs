﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Utils;

public sealed class Squad : BareObserver<Squad.Actions>
{
    public enum Actions { UNIT_ADDED, UNIT_REMOVED }

    public object UserData = null;

    #region Private Attributes
    private Dictionary<DataType, ISquadData> _data = new Dictionary<DataType, ISquadData>();
    private List<Unit> _units = new List<Unit>();

    private AutoUnregister _auto;
    #endregion

    #region Public Getters/Setters
    public List<Unit> Units
    {
        get
        {
            return _units.ToList();
        }
    }

    // TODO: Precach all
    public List<Selectable> Selectables
    {
        get
        {
            return _units.Select(x => x.GetComponent<Selectable>()).ToList();
        }
    }

    public BoundingBox.BoundingBoxHolder BoundingBox
    {
        get
        {
            return ((BoundingBox)_data[DataType.BOUNDING_BOX]).Value;
        }
    }

    public float Attack
    {
        get
        {
            return ((AttackValue)_data[DataType.ATTACK_VALUE]).Value;
        }
    }

    public Squad EnemySquad
    {
        get
        {
            return ((SmelledEnemies)_data[DataType.SMELLED_ENEMIES]).Value;
        }
    }

    public int PatrolPosition
    {
        get
        {
            return ((PatrolData)_data[DataType.PATROL_DATA]).PatrolPosition;
        }
        set
        {
            ((PatrolData)_data[DataType.PATROL_DATA]).PatrolPosition = value;
        }
    }

    public Squad AssistSquad
    {
        get
        {
            return ((AssistData)_data[DataType.ASSIST_DATA]).AssistSquad;
        }
        set
        {
            ((AssistData)_data[DataType.ASSIST_DATA]).AssistSquad = value;
        }
    }
    private Storage.Races _race;
    public Storage.Races Race
    {
        get
        {
            return _race;
        }
    }

    // FIXME: Use the squad race :/
    private float _maxAttackRange;
    public float MaxAttackRange
    {
        get
        {
            return _maxAttackRange;
        }
    }
    public bool NotMoved
    {
        get
        {
            return ((BoundingBox)_data[DataType.BOUNDING_BOX]).notMoved;
        }
    }
    #endregion

    public Squad(Storage.Races race, bool smellEnemies = true)
    {
        _auto = new AutoUnregister(this);
        _race = race;

        _data.Add(DataType.BOUNDING_BOX, new BoundingBox(this));
        _data.Add(DataType.ATTACK_VALUE, new AttackValue(this));
        _data.Add(DataType.PATROL_DATA, new PatrolData(this));
        _data.Add(DataType.ASSIST_DATA, new AssistData(this));

        // Smelled squads can not smell enemies or we would run into endless recursion
        if (smellEnemies)
        {
            _maxAttackRange = Storage.Info.get.of(_race, Storage.UnitTypes.THROWN).unitAttributes.rangedAttackFurthest;
            _data.Add(DataType.SMELLED_ENEMIES, new SmelledEnemies(this));
        }
        else
        {
            _maxAttackRange = 1f;
        }
    }
    //used for AI so smellEnemies is always true
    public Squad(Storage.Races race, int error)
    {
        _auto = new AutoUnregister(this);
        _race = race;

        _data.Add(DataType.BOUNDING_BOX, new BoundingBox(this));
        _data.Add(DataType.ATTACK_VALUE, new AttackValue(this,error));
        _data.Add(DataType.PATROL_DATA, new PatrolData(this));
        _data.Add(DataType.ASSIST_DATA, new AssistData(this));

        _maxAttackRange = Storage.Info.get.of(_race, Storage.UnitTypes.THROWN).unitAttributes.rangedAttackFurthest;
        _data.Add(DataType.SMELLED_ENEMIES, new SmelledEnemies(this));

    }
    private void OnUnitDied(System.Object obj)
    {
        RemoveUnit(((GameObject)obj).GetComponent<Unit>());
    }

    public void AddUnit(Unit unit)
    {
        if (!_units.Contains(unit))
        {
            _units.Add(unit);
            _auto += unit.registerFatalWounds(OnUnitDied);

            fire(Actions.UNIT_ADDED, unit);
        }
    }

    public void AddUnits(IEnumerable<Unit> units)
    {
        foreach (Unit unit in units)
        {
            AddUnit(unit);
        }
    }

    public void AddUnits(Squad squad)
    {
        AddUnits(squad.Units);
    }

    public void RemoveUnit(Unit unit)
    {
        _units.Remove(unit);
        _auto -= unit.unregisterFatalWounds(OnUnitDied);

        fire(Actions.UNIT_REMOVED, unit);
    }

    public void UpdateUnits(IEnumerable<Unit> units)
    {
        foreach (Unit unit in units)
        {
            if (!_units.Contains(unit))
            {
                AddUnit(unit);
            }
        }

        foreach (Unit unit in _units.ToList())
        {
            if (!units.Contains(unit))
            {
                RemoveUnit(unit);
            }
        }
    }

    public void MoveTo(Vector3 target, Action<Unit> callback = null)
    {
        foreach (Unit unit in _units)
        {
            unit.moveTo(target);

            if (callback != null)
            {
                callback(unit);
            }
        }
    }

    public void AttackTo(IGameEntity target, Action<Unit> callback = null)
    {
        foreach (Unit unit in _units)
        {
            unit.attackTarget(target);

            if (callback != null)
            {
                callback(unit);
            }
        }
    }

    public void EnterTo(IGameEntity target, Action<Unit> callback = null)
    {
        foreach (Unit unit in _units)
        {
            if (unit.info.isCivil)
            {
                unit.goToBuilding(target);

                if (callback != null)
                {
                    callback(unit);
                }
            }
        }
    }

    public void Update()
    {
        foreach (var entry in _data)
        {
            // TODO: Smarter BoundingBox update, only when moving
            if (entry.Key == DataType.BOUNDING_BOX)
            {
                entry.Value.ForceUpdate();
            }

            entry.Value.Update(_units);
        }
    }

    public override void OnDestroy()
    {
        foreach (var entry in _data)
        {
            entry.Value.OnDestroy();
        }

        base.OnDestroy();
    }
}

public sealed class SquadUpdater : GameEntity<SquadUpdater.DummyActions>
{
    public enum DummyActions { }

    private EntityStatus _defaultStatus = EntityStatus.IDLE;
    public override EntityStatus DefaultStatus
    {
        get
        {
            return _defaultStatus;
        }
        set
        {
            _defaultStatus = value;
        }
    }

    #region Not Implemented Overrides
    public override E getType<E>() { throw new NotImplementedException(); }

    public override IKeyGetter registerFatalWounds(Action<object> func)
    {
        throw new NotImplementedException();
    }

    public override IKeyGetter unregisterFatalWounds(Action<object> func)
    {
        throw new NotImplementedException();
    }

    protected override void onFatalWounds()
    {
        throw new NotImplementedException();
    }

    protected override void onReceiveDamage()
    {
        throw new NotImplementedException();
    }
    #endregion

    private Squad _squad;
    public Squad UnitsSquad { get { return _squad; } }

    public void Initialize(Storage.Races race)
    {
        _squad = new Squad(race);
    }

    public override void Awake() { }
    public override void Start() { }

    public override void Update()
    {
        base.Update();
        _squad.Update();
    }

    public override void OnDestroy()
    {
        base.OnDestroy();
        _squad.OnDestroy();
    }
}

public enum DataType { BOUNDING_BOX, ATTACK_VALUE, SMELLED_ENEMIES, PATROL_DATA, ASSIST_DATA }

public interface ISquadData
{
    bool NeedsUpdate { get; }
    void Update(List<Unit> units);
    void ForceUpdate();
    void OnDestroy();
}

public abstract class SquadData<T> : BareObserver<SquadData<T>.Actions>, ISquadData
{
    public enum Actions { }

    public abstract T Value { get; }

    protected bool _needsUpdate = true;
    public bool NeedsUpdate { get { return _needsUpdate; } }
    public abstract void Update(List<Unit> units);

    private AutoUnregister auto;
    public SquadData(Squad squad)
    {
        auto = new AutoUnregister(this);
        auto += squad.register(Squad.Actions.UNIT_ADDED, OnAdded);
        auto += squad.register(Squad.Actions.UNIT_REMOVED, OnRemoved);
    }

    public void ForceUpdate() { _needsUpdate = true; }
    public virtual void OnAdded(System.Object obj) { _needsUpdate = true; }
    public virtual void OnRemoved(System.Object obj) { _needsUpdate = true; }
}

public class BoundingBox : SquadData<BoundingBox.BoundingBoxHolder>
{
    public class BoundingBoxHolder
    {
        public Rect Bounds;
        public float MaxLongitude;
    }

    private BoundingBoxHolder _boundingBox = new BoundingBoxHolder();
    public override BoundingBoxHolder Value { get { return _boundingBox; } }
    public bool notMoved { get; set; }
    public BoundingBox(Squad squad) : base(squad)
    {
        notMoved = true;
    }

    public static Rect CalculateOf(List<Unit> units)
    {
        float minX = Mathf.Infinity;
        float maxX = -Mathf.Infinity;
        float minY = Mathf.Infinity;
        float maxY = -Mathf.Infinity;

        foreach (Unit unit in units)
        {
            Vector3 position = unit.transform.position;

            if (maxY < position.z) maxY = position.z;
            if (minY > position.z) minY = position.z;
            if (maxX < position.x) maxX = position.x;
            if (minX > position.x) minX = position.x;
        }

        return new Rect(minX, minY, (maxX - minX) * 2, (maxY - minY) * 2);
    }

    public override void Update(List<Unit> units)
    {
        if (_needsUpdate)
        {
            Rect boundingBox = CalculateOf(units);
            notMoved = (Vector2.Distance(boundingBox.center, _boundingBox.Bounds.center)<2);
            _boundingBox.Bounds = boundingBox;
            _boundingBox.MaxLongitude = boundingBox.width > boundingBox.height ? boundingBox.height : boundingBox.width;
            _boundingBox.MaxLongitude = _boundingBox.MaxLongitude < 1f ? 1f : _boundingBox.MaxLongitude;

            _needsUpdate = false;
        }
    }
}

public class AttackValue : SquadData<float>
{
    private float _attack;
    public override float Value { get { return _attack; } }
    private int error;
    public AttackValue(Squad squad) : base(squad)
    {
        error = 0;
    }
    public AttackValue(Squad squad,int error) : base(squad)
    {
        this.error = error;
    }
    //If this ever becomes too resource consuming we can precalculate it for each unit.
    public static float OfUnit (Unit unit,int error)
    {
        Storage.UnitAttributes a = unit.info.unitAttributes;
        return Mathf.Max(200, ((Mathf.Pow(2, a.strength * 1.05f)) +
               Mathf.Pow(2, Mathf.Max(
                   (a.projectileAbility),
                   (a.weaponAbility * 0.8f))) *
                   (a.attackRange * 0.2f) + a.resistance + a.wounds+(getError(error)))) *
                   unit.healthPercentage * 0.003f;
    }
    /// <summary>
    /// Outputs a different error based on the Ai difficulty level, only the easier difficulties should make errors
    /// errors will be around these levels:
    ///     difficultyLvl, error
    ///                 1: 1.20
    ///                 2: 0.35
    ///                 3: 0 (always 0)
    /// </summary>
    /// <returns></returns>
    private static float getError(int difficultyLvl)
    {
        int error = Utils.D6.get.rollOnce();
        if (error > (difficultyLvl + 3))
            return 6 / (difficultyLvl + 1);
        return 0;
    }

    public override void Update(List<Unit> units)
    {
        if (_needsUpdate)
        {
            _attack = 0;
            foreach (Unit unit in units)
            {
                _attack += OfUnit(unit,error);
            }
            _needsUpdate = false;
        }
    }
}

public class SmelledEnemies : SquadData<Squad>
{
    private Squad _ownSquad;
    private Squad _enemySquad = new Squad(Storage.Races.RESERVED_UNSPECIFIED, false);
    public override Squad Value { get { return _enemySquad; } }

    public SmelledEnemies(Squad squad) : base(squad)
    {
        _ownSquad = squad;
    }

    public override void Update(List<Unit> units)
    {
        if (units.Count > 0)
        {
            BoundingBox.BoundingBoxHolder boundingBox = _ownSquad.BoundingBox;

#if UNITY_EDITOR
            Debug.DrawLine(new Vector3(boundingBox.Bounds.x, units[0].transform.position.y, boundingBox.Bounds.y), new Vector3(boundingBox.Bounds.x + boundingBox.MaxLongitude, units[0].transform.position.y, boundingBox.Bounds.y + boundingBox.MaxLongitude), Color.white);
#endif

            List<Unit> enemyUnits = Helpers.getVisibleUnitsNotOfRaceNearPosition(new Vector3(boundingBox.Bounds.x, units[0].transform.position.y, boundingBox.Bounds.y), boundingBox.MaxLongitude * 3 * _ownSquad.MaxAttackRange, _ownSquad.Race);
            _enemySquad.UpdateUnits(enemyUnits);
            _enemySquad.Update();
        }
    }
}

public class PatrolData : SquadData<int>
{
    public int PatrolPosition { get; set; }
    public override int Value { get { return PatrolPosition; } }
    public PatrolData(Squad squad) : base(squad) {
        PatrolPosition = 0;
    }
    public override void Update(List<Unit> units)
    {
        //updated externally
    }
}

public class AssistData : SquadData<Squad>
{
    public Squad AssistSquad { get; set; }
    public override Squad Value { get { return AssistSquad; } }
    public AssistData(Squad squad) : base(squad)
    {
        AssistSquad = null;
    }
    public override void Update(List<Unit> units)
    {
        //updated externally
    }
}