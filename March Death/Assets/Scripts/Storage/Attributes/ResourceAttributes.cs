using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Storage
{

    public sealed class ResourceAttributes : BuildingAttributes
    {
        public int storeSize;
        public int maxUnits;
        public int productionRate;
        public float updateInterval;
        public float trapRange;
        

    }
}